var LISTEN_IP = '127.0.0.1'; 
var LISTEN_PORT = 8086; 
var DEFAULT_FILE = "robot_web_first.html"; 

var http = require('http'), 
    fs = require('fs'); 
var gpio = require("pi-gpio");

function getExtension(fileName) { 
    var fileNameLength = fileName.length; 
    var dotPoint = fileName.indexOf('.', fileNameLength - 5 ); 
    var extn = fileName.substring(dotPoint + 1, fileNameLength); 
    return extn; 
} 

function getContentType(fileName) { 
    var extentsion = getExtension(fileName).toLowerCase(); 
    var contentType = { 
        'html': 'text/html', 
        'htm' : 'text/htm', 
        'css' : 'text/css', 
        'js' : 'text/javaScript; charset=utf-8', 
        'json' : 'application/json; charset=utf-8', 
        'xml' : 'application/xml; charset=utf-8', 
        'jpeg' : 'image/jpeg', 
        'jpg' : 'image/jpg', 
        'gif' : 'image/gif', 
        'png' : 'image/png', 
        'mp3' : 'audio/mp3', 
        }; 
        var contentType_value = contentType[extentsion]; 
        if(contentType_value === undefined){ 
            contentType_value = 'text/plain';}; 
    return contentType_value; 
} 

function left(response){
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Moving left\n'); 
	response.end(); 
}

function forward(response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Moving forward\n'); 
	response.end(); 
}

function right(response){
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Moving right\n'); 
	response.end(); 
}

function back(response){
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Moving back\n'); 
	response.end(); 
}

function scoop(response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Taking scoop\n'); 
	response.end(); 
}

function fForward(response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Running forward\n'); 
	response.end(); 
}

function fBack(response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Running backward\n'); 
	response.end(); 
}

function lift(response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Lifting\n'); 
	response.end(); 
}

function stop(response) {
	response.writeHead(200, {'Content-Type': 'text/plain'}); 
	response.write('Stopping\n'); 
	response.end(); 
}

var funcMapping = {
	'/move/forward/forward' : forward,
	'/move/forward/left': left,
	'/move/forward/back': back,
	'/move/forward/right': right,
	'/move/forward/scoop': scoop,
	'/move/forward/fback': fBack,
	'/move/forward/fforward': fForward,
	'/move/forward/lift': lift,
	'/move/forward/stop' : stop
}

var server  = http.createServer(); 
server.on('request', 
    function(request, response){ 
        console.log('Requested Url:' + request.url); 
        var requestedFile = request.url; 
        requestedFile = (requestedFile.substring(requestedFile.length - 1, 1) === '/')?requestedFile + DEFAULT_FILE : requestedFile; 
        console.log('Handle Url:' + requestedFile); 
        console.log('File Extention:' + getExtension( requestedFile));
        console.log('Content-Type:' + getContentType( requestedFile));
		
		if(typeof funcMapping[request.url] != 'undefined'){
			funcMapping[request.url](response);
		}else{
			fs.readFile('.' + requestedFile,'binary', function (err, data) { 
				if(err){ 
					response.writeHead(404, {'Content-Type': 'text/plain'}); 
					response.write('not found\n'); 
					response.end();    
				}else{ 
					response.writeHead(200, {'Content-Type': getContentType(requestedFile)}); 
					response.write(data, "binary"); 
					response.end(); 
				} 
			});
		}
    } 
); 




server.listen(LISTEN_PORT, LISTEN_IP); 
console.log('Server running at http://' + LISTEN_IP + ':' + LISTEN_PORT); 